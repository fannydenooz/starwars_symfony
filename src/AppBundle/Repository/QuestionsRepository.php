<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class QuestionsRepository extends EntityRepository
{
    public function getQuestionNotIn($listquestions)
    {
        $qb = $this->createQueryBuilder('q');

        if($listquestions!=null)
        {
            $qb->where('q.idquestion NOT IN (:ids)') //choose a question not already asked
            ->setParameter('ids', $listquestions, \Doctrine\DBAL\Connection::PARAM_INT_ARRAY);
        }       

        $items=$qb->getQuery()->getResult();

        $ramdomquestion=$items[array_rand($items)];

        return $ramdomquestion;

    }
}