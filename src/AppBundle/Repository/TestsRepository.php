<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class TestsRepository extends EntityRepository
{
    public function getIncompleteTest($user)
    {
        $qb = $this->createQueryBuilder('t');
        
        $qb->where('t.resultat IS NULL')
        ->andWhere('t.user = :user')
        ->setParameter('user', $user);

        return $qb
            ->getQuery()->getResult();

    }

    public function getAskedQuestion($test)
    {
        $qb=$this->_em->createQueryBuilder();

        $questionsUser = $qb
            ->select('q.idquestion')
            ->from($this->_entityName, 't')
            ->innerJoin('t.testsreponses', 'tr')
            ->innerJoin('tr.reponse', 'r')
            ->innerJoin('r.question', 'q')
            ->where('t = :test')
            ->setParameter('test', $test);

        $questions=$qb->getQuery()->getResult();

        return $questions;
    }

    public function getNumberQuestion($test)
    {
        $qb=$this->_em->createQueryBuilder();

        $questionsUser = $qb
            ->select('count(tr.idtestsreponses)')
            ->from($this->_entityName, 't')
            ->innerJoin('t.testsreponses', 'tr')
            ->where('t = :test')
            ->setParameter('test', $test);

        $count = $qb->getQuery()->getSingleScalarResult();

        return $count;
    }

    public function getTestResult($test)
    {
        $qb=$this->_em->createQueryBuilder();

        $questionsUser = $qb
            ->select('count(r.idreponse)')
            ->from($this->_entityName, 't')
            ->innerJoin('t.testsreponses', 'tr')
            ->innerJoin('tr.reponse', 'r')
            ->where('r.bonnereponse = true')
            ->andWhere('t = :test')
            ->setParameter('test', $test);

        $result = $qb->getQuery()->getSingleScalarResult();

        return $result;
    }


    /*
    $groups = $this->getDoctrine()->getManager()->createQueryBuilder()
    ->select('g')
    ->from('BundleName:Group', 'g')
    ->innerJoin('g.groupMembers', 'gm')
    ->innerJoin('gm.user', 'u')
    ->where('u = :user')
    ->setParameter('user', $userObject)
    ->getQuery()
    ->getResult();
    */


    /*$qb->innerJoin('t.testsreponses', 'tr')
    ->innerJoin('tr.reponse', 'r')
    ->innerJoin('r.question', 'q')*/

}