<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class PlanetesRepository extends EntityRepository
{
    public function findAll()
    {
        return $this->findBy(array(), array('nomplanete' => 'ASC'));
    }

    public function search($filter)
    {
        $filter = '%'.$filter.'%';
        $qb = $this->createQueryBuilder('t');

        $qb ->where('t.nomplanete LIKE :filter')
            ->setParameter('filter', $filter)
            ->orderBy('t.nomplanete', 'ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }
}