<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class FilmsRepository extends EntityRepository
{
    public function getFilmFromTemporalite($temporalite)
    {
        $qb = $this->createQueryBuilder('f');

        $qb->leftJoin('f.temporalite', 't')
            ->where('t.nomtemporalite LIKE :temporalite')
            ->setParameter('temporalite', $temporalite)
            ->orderBy('f.annee', 'ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult()
        ;

    }

    public function search($filter, $temporalite)
    {
        $filter = '%'.$filter.'%';
        $qb = $this->createQueryBuilder('f');

        $qb ->leftJoin('f.temporalite', 't')
            ->where('t.nomtemporalite LIKE :temporalite')
            ->andWhere('f.nomfilm LIKE :filter')
            ->setParameter('temporalite', $temporalite)
            ->setParameter('filter', $filter)
            ->orderBy('f.annee', 'ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }
    
}