<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;

class PersonnagesRepository extends EntityRepository
{
    public function getPersonnagesFromType($type)
    {
        $qb = $this->createQueryBuilder('p');

        $qb->leftJoin('p.type', 't')
            ->where('t.nomtype LIKE :type')
            ->setParameter('type', $type)
            ->orderBy('p.nom', 'ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult()
        ;

    }

    public function search($filter, $type)
    {
        $filter = '%'.$filter.'%';
        $qb = $this->createQueryBuilder('p');

        $qb ->leftJoin('p.type', 't')
            ->where('t.nomtype LIKE :type')
            ->andWhere('p.nom LIKE :filter')
            ->setParameter('type', $type)
            ->setParameter('filter', $filter)
            ->orderBy('p.nom', 'ASC')
        ;

        return $qb
            ->getQuery()
            ->getResult()
        ;
    }
}