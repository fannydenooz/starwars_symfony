<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Doctrine\DBAL\Event\Listeners\MysqlSessionInit;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use UserBundle\Form\UserType;
use AppBundle\Entity\Users;
use AppBundle\Entity\Planetes;
use AppBundle\Entity\Questions;
use AppBundle\Entity\Personnages;
use AppBundle\Entity\Films;
use AppBundle\Form\PlanetesType;
use AppBundle\Form\QuestionsType;
use AppBundle\Form\PersonnagesType;
use AppBundle\Form\FilmsType;
use AppBundle\Entity\Reponses;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\User\UserInterface;
use AppBundle\Entity\TestsReponses;
use AppBundle\Entity\Tests;

class DefaultController extends Controller
{
    
    public function indexAction()
    {
        return $this->render("@App/Default/index.html.twig"); 
    }

    public function personnagesAction()
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Types');
        
        $JediType = $repository->findOneBynomtype('Jedi/Alliance Rebelle');
        $SithType = $repository->findOneBynomtype('Sith');
        $AutreType = $repository->findOneBynomtype('Autre');

        $images = array(
            'jedi' => $JediType->getUrlimage(),
            'sith' => $SithType->getUrlimage(),
            'autre' => $AutreType->getUrlimage()
        );

        return $this->render("@App/Default/personnages.html.twig", $images);
    }

    public function jediAction(Request $request)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Personnages');

        if($request->isMethod('POST') && $request->get('search'))
        {
            $listPersonnages = $repository->search($request->get('search'), 'Jedi%');
        }
        else
        {
            $listPersonnages = $repository->getPersonnagesFromType('Jedi%');
        }

        return $this->render("@App/Default/type_personnages.html.twig", array('listPersonnages' => $listPersonnages, 'type' => 'Jedi'));
    }

    public function sithAction(Request $request)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Personnages');
    
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Personnages');

        if($request->isMethod('POST') && $request->get('search'))
        {
            $listPersonnages = $repository->search($request->get('search'), 'Sith%');
        }
        else
        {
            $listPersonnages = $repository->getPersonnagesFromType('Sith%');
        }

        return $this->render("@App/Default/type_personnages.html.twig", array('listPersonnages' => $listPersonnages, 'type' => 'Sith'));
    }

    public function autresAction(Request $request)
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Personnages');
    
        if($request->isMethod('POST') && $request->get('search'))
        {
            $listPersonnages = $repository->search($request->get('search'), 'Autre%');
        }
        else
        {
            $listPersonnages = $repository->getPersonnagesFromType('Autre%');
        }

        return $this->render("@App/Default/type_personnages.html.twig", array('listPersonnages' => $listPersonnages, 'type' => 'Autre'));
    }

    public function planetesAction(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Planetes');

        if($request->isMethod('POST') && $request->get('search'))
        {
            $listPlanetes = $repository->search($request->get('search'));
        }
        else
        {
            $listPlanetes = $repository->findAll();
        }
        
        return $this->render("@App/Default/planetes.html.twig", array('listPlanetes' => $listPlanetes));
    }

    public function filmsAction()
    {
        $repository = $this
        ->getDoctrine()
        ->getManager()
        ->getRepository('AppBundle:Temporalites');
    
    $FilmsDerives = $repository->findOneBynomtemporalite('Films dérivés');
    $Prelogie = $repository->findOneBynomtemporalite('Prelogie');
    $TrilogieOriginale = $repository->findOneBynomtemporalite('Trilogie originale');
    $TroisiemeTrilogie = $repository->findOneBynomtemporalite('Troisième trilogie');

    $images = array(
        'FilmsDerives' => $FilmsDerives->getUrlimage(),
        'Prelogie' => $Prelogie->getUrlimage(),
        'TrilogieOriginale' => $TrilogieOriginale->getUrlimage(),
        'TroisiemeTrilogie' => $TroisiemeTrilogie->getUrlimage()
    );

        return $this->render("@App/Default/films.html.twig", $images);
    }

    public function trilogieOriginaleAction(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Films');

        if($request->isMethod('POST') && $request->get('search'))
        {
            $listFilms = $repository->search($request->get('search'), 'Trilogie originale');
        }
        else
        {
            $listFilms = $repository->getFilmFromTemporalite('Trilogie originale');
        }
        
        return $this->render("@App/Default/temporalite_films.html.twig" , array('listFilms' => $listFilms, 'temporalite' => 'Trilogie originale'));
    }

    public function prelogieAction(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Films');

        if($request->isMethod('POST') && $request->get('search'))
        {
            $listFilms = $repository->search($request->get('search'), 'Prelogie');
        }
        else
        {
            $listFilms = $repository->getFilmFromTemporalite('Prelogie');
        }

        return $this->render("@App/Default/temporalite_films.html.twig" , array('listFilms' => $listFilms, 'temporalite' => 'Prelogie'));
    }

    public function troisiemeTrilogieAction(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Films');

        if($request->isMethod('POST') && $request->get('search'))
        {
            $listFilms = $repository->search($request->get('search'), 'Troisième trilogie');
        }
        else
        {
            $listFilms = $repository->getFilmFromTemporalite('Troisième trilogie');
        }
        
        return $this->render("@App/Default/temporalite_films.html.twig" , array('listFilms' => $listFilms, 'temporalite' => 'Troisième trilogie'));
    }

    public function filmsDerivesAction(Request $request)
    {
        $repository = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Films');
        
        if($request->isMethod('POST') && $request->get('search'))
        {
            $listFilms = $repository->search($request->get('search'), 'Films dérivés');
        }
        else
        {
            $listFilms = $repository->getFilmFromTemporalite('Films dérivés');
        }

        return $this->render("@App/Default/temporalite_films.html.twig" , array('listFilms' => $listFilms, 'temporalite' => 'Films dérivés'));
    }

    /**
   * @Security("has_role('ROLE_USER')")
   */
    public function questionnaireAction(Request $request, UserInterface $user = null)
    {
        $repositoryTests = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Tests');

        $repositoryQuestions = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Questions');

        $tests=$repositoryTests->getIncompleteTest($user->getId());

        if(!empty($tests)) // un test est déjà en cours
        {
            $test=$tests[0]; 

            if($request->isMethod('POST') && $request->get('radio'))
            {
                $idreponse=$request->get('radio');

                $repositoryReponses = $this
                ->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Reponses');

                $userReponse=$repositoryReponses->findOneByidreponse($idreponse);

                $testsReponse= new TestsReponses();
                $testsReponse->setReponse($userReponse);
                $testsReponse->setTest($test);

                $em = $this->getDoctrine()->getManager();
                $em->persist($testsReponse);
                $em->flush();

                $questionNumber=$repositoryTests->getNumberQuestion($test);

                if($questionNumber==20) //Le test est fini
                {
                    $test->setResultat($repositoryTests->getTestResult($test));
                    $em->flush();

                    return $this->render("@App/Default/resultat.html.twig" , array('test' => $test));
                }

            }
            
            $questionNumber=$repositoryTests->getNumberQuestion($test);

            $askedQuestions=$repositoryTests->getAskedQuestion($test);
            $question = $repositoryQuestions->getQuestionNotIn($askedQuestions);

        }
        else //pas de test en cours, créé un nouveau test
        {
            $repositoryUser = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('UserBundle:User');

            $test=new Tests();
            $test->setUser($repositoryUser->findOneByid($user->getId()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($test);
            $em->flush();

            $question = $repositoryQuestions->getQuestionNotIn(null);
            $questionNumber=0;
        }
        
        return $this->render("@App/Default/questionnaire.html.twig" , array('question' => $question, 'questionNumber' => $questionNumber+1));
    }

    /**
   * @Security("has_role('ROLE_ADMIN')")
   */
    public function resultatAction(Request $request)
    {
        $repositoryTests = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Tests');

        $idTest = $request->query->get('test');

        $test = $repositoryTests->findOneByidtest($idTest);

        return $this->render("@App/Default/resultat.html.twig" , array('test' => $test));
    }

    public function loginAction()
    {
        return $this->render("@App/Default/login.html.twig");
    }

    public function signupAction(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        // 1) build the form
        $user = new Users();
        $form = $this->createForm(UserType::class, $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // 3) Encode the password (you could also do this via Doctrine listener)
            $password = $passwordEncoder->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            // 4) save the User!
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // ... do any other work - like sending them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('app_homepage');
        }

        return $this->render(
            '@App/Default/signup.html.twig',
            array('form' => $form->createView())
        );

    }

    //Pages admin

    public function administrationAction()
    {
        return $this->render("@App/Default/administration.html.twig"); 
    }

    public function addPlaneteAction(Request $request)
    {
        if($request->get('planeteid'))
        {
            $planete = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Planetes')
                ->find($request->get('planeteid'))
                ;           
        }
        else
        {
            $planete = new Planetes();
        }            

        $form = $this->createForm(PlanetesType::class, $planete);

        if ($request->isMethod('POST')) 
        {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $planete contient les valeurs entrées dans le formulaire par le visiteur
            $form->handleRequest($request);
    
            // On vérifie que les valeurs entrées sont correctes
            if ($form->isValid()) 
            {
                // On enregistre dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($planete);
                $em->flush();
        
                $request->getSession()->getFlashBag()->add('notice', 'Enregistré!!!');
        
                // On redirige vers la page de visualisation 
                return $this->redirectToRoute('app_adminPlanetes');
            }
        }
    
        // À ce stade, le formulaire n'est pas valide car :
        // - Soit la requête est de type GET, donc le visiteur vient d'arriver sur la page et veut voir le formulaire
        // - Soit la requête est de type POST, mais le formulaire contient des valeurs invalides, donc on l'affiche de nouveau
        return $this->render('@App/Default/admin_form.html.twig', array(
            'form' => $form->createView(),
            ));
    
    }

    public function addQuestionAction(Request $request)
    {
        if($request->get('questionid'))
        {
            $question = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Questions')
                ->find($request->get('questionid'))
                ;           
        }
        else
        {
            $question = new Questions();
        }     

        $form = $this->createForm(QuestionsType::class, $question);

        if ($request->isMethod('POST') && $form->handleRequest($request)->isValid()) 
        {
            $em = $this->getDoctrine()->getManager();

            foreach ($question->getReponses() as $reponse) 
            {
                $reponse->setQuestion($question);
            }

            $em->persist($question);
            $em->flush();
    
            $request->getSession()->getFlashBag()->add('notice', 'Enregistré!!!');

            return $this->redirectToRoute('app_adminQuestionnaire');
        }
    
        return $this->render('@App/Default/admin_question_form.html.twig', array(
            'form' => $form->createView(),
            ));
    
    }
/***********************************A MODIFIER************************************************ */

    public function addPersonnageAction(Request $request)
    {
        if($request->get('personnageid'))
        {
            $personnage = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Personnages')
                ->find($request->get('personnageid'))
                ;           
        }
        else
        {
            $personnage = new Personnages();
        }            

        $form = $this->createForm(PersonnagesType::class, $personnage);

        if ($request->isMethod('POST')) 
        {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $planete contient les valeurs entrées dans le formulaire par le visiteur
            $form->handleRequest($request);
    
            // On vérifie que les valeurs entrées sont correctes
            if ($form->isValid()) 
            {
                // On enregistre dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($personnage);
                $em->flush();
        
                $request->getSession()->getFlashBag()->add('notice', 'Enregistré!!!');
        
                // On redirige vers la page de visualisation 
                return $this->redirectToRoute('app_adminPersonnages');
            }
        }
    
        // À ce stade, le formulaire n'est pas valide car :
        // - Soit la requête est de type GET, donc le visiteur vient d'arriver sur la page et veut voir le formulaire
        // - Soit la requête est de type POST, mais le formulaire contient des valeurs invalides, donc on l'affiche de nouveau
        return $this->render('@App/Default/admin_form.html.twig', array(
            'form' => $form->createView(),
            ));
    
    }


    public function addFilmAction(Request $request)
    {
        if($request->get('filmid'))
        {
            $film = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Films')
                ->find($request->get('filmid'))
                ;           
        }
        else
        {
            $film = new Films();
        }            

        $form = $this->createForm(FilmsType::class, $film);

        if ($request->isMethod('POST')) 
        {
            // On fait le lien Requête <-> Formulaire
            // À partir de maintenant, la variable $planete contient les valeurs entrées dans le formulaire par le visiteur
            $form->handleRequest($request);
    
            // On vérifie que les valeurs entrées sont correctes
            if ($form->isValid()) 
            {
                // On enregistre dans la base de données
                $em = $this->getDoctrine()->getManager();
                $em->persist($film);
                $em->flush();
        
                $request->getSession()->getFlashBag()->add('notice', 'Enregistré!!!');
        
                // On redirige vers la page de visualisation 
                return $this->redirectToRoute('app_adminFilms');
            }
        }
    
        // À ce stade, le formulaire n'est pas valide car :
        // - Soit la requête est de type GET, donc le visiteur vient d'arriver sur la page et veut voir le formulaire
        // - Soit la requête est de type POST, mais le formulaire contient des valeurs invalides, donc on l'affiche de nouveau
        return $this->render('@App/Default/admin_form.html.twig', array(
            'form' => $form->createView(),
            ));
    
    }

    /********************************************************************************************************* */

    public function adminPersonnagesAction(Request $request)
    {
        if($request->isMethod('POST') && $request->get('adminaction'))
        {
            $action = $request->get('adminaction');
            if($action=='Ajouter')
            {
                return $this->redirectToRoute('app_addpersonnage');
            }
            else if($action=='Supprimer')
            {
                $personnage = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Personnages')
                ->find($request->get('id'))
                ;     

                $em = $this->getDoctrine()->getManager();
                $em->remove($personnage);
                $em->flush();

                $request->getSession()->getFlashBag()->add('notice', 'Supprimé!!!');

                return $this->redirectToRoute('app_adminPersonnages');
            }
            else if($action=='Modifier')
            {
                return $this->redirectToRoute('app_addpersonnage', array('personnageid' => $request->get('id')));
            }

        }
        else
        {
            $repositoryPersonnages = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Personnages');

            $listPersonnages=$repositoryPersonnages->findAll();

            return $this->render('@App/Default/admin_view.html.twig', array('elements' => $listPersonnages, 'type' => 'Personnage'));
        }



    }

    public function adminFilmsAction(Request $request)
    {
        if($request->isMethod('POST') && $request->get('adminaction'))
        {
            $action = $request->get('adminaction');
            if($action=='Ajouter')
            {
                return $this->redirectToRoute('app_addfilm');
            }
            else if($action=='Supprimer')
            {
                $film = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Films')
                ->find($request->get('id'))
                ;     

                $em = $this->getDoctrine()->getManager();
                $em->remove($film);
                $em->flush();

                $request->getSession()->getFlashBag()->add('notice', 'Supprimé!!!');

                return $this->redirectToRoute('app_adminFilms');
            }
            else if($action=='Modifier')
            {
                return $this->redirectToRoute('app_addfilm', array('filmid' => $request->get('id')));
            }

        }
        else
        {
            $repositoryFilms = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Films');

            $listFilms=$repositoryFilms->findAll();

            return $this->render('@App/Default/admin_view.html.twig', array('elements' => $listFilms, 'type' => 'Film'));
        }


    }

    public function adminPlanetesAction(Request $request)
    {
        if($request->isMethod('POST') && $request->get('adminaction'))
        {
            $action = $request->get('adminaction');
            if($action=='Ajouter')
            {
                return $this->redirectToRoute('app_addplanete');
            }
            else if($action=='Supprimer')
            {
                $planete = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Planetes')
                ->find($request->get('id'))
                ;     

                $em = $this->getDoctrine()->getManager();
                $em->remove($planete);
                $em->flush();

                $request->getSession()->getFlashBag()->add('notice', 'Supprimé!!!');

                return $this->redirectToRoute('app_adminPlanetes');
            }
            else if($action=='Modifier')
            {
                return $this->redirectToRoute('app_addplanete', array('planeteid' => $request->get('id')));
            }

        }
        else
        {
            $repositoryPlanetes = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Planetes');

            $listPlanetes=$repositoryPlanetes->findAll();

            return $this->render('@App/Default/admin_view.html.twig', array('elements' => $listPlanetes, 'type' => 'Planete'));
        }
        
    }

    public function adminQuestionnaireAction(Request $request)
    {
        if($request->isMethod('POST') && $request->get('adminaction'))
        {
            $action = $request->get('adminaction');
            if($action=='Ajouter')
            {
                return $this->redirectToRoute('app_addquestion');
            }
            else if($action=='Supprimer')
            {
                $question = $this->getDoctrine()
                ->getManager()
                ->getRepository('AppBundle:Questions')
                ->find($request->get('id'))
                ;     

                $em = $this->getDoctrine()->getManager();
                $em->remove($question);
                $em->flush();

                $request->getSession()->getFlashBag()->add('notice', 'Supprimé!!!');

                return $this->redirectToRoute('app_adminQuestionnaire');
            }
            else if($action=='Modifier')
            {
                return $this->redirectToRoute('app_addquestion', array('questionid' => $request->get('id')));
            }

        }
        else
        {
            $repositoryQuestions = $this
            ->getDoctrine()
            ->getManager()
            ->getRepository('AppBundle:Questions');

            $listQuestions=$repositoryQuestions->findAll();

            return $this->render('@App/Default/admin_view.html.twig', array('elements' => $listQuestions, 'type' => 'Question'));
        }


    }
    
}
