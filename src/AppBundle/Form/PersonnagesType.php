<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class PersonnagesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom',      TextType::class)
            ->add('file',     FileType::class)
            ->add('description',   TextareaType::class)
            ->add('type',   EntityType::class, array(
                'class'        => 'AppBundle:Types',
                'choice_label' => 'nomtype',
                'multiple'     => false,
                'expanded'     => false,
              ))
            ->add('planeteorigine',   EntityType::class, array(
                'class'        => 'AppBundle:Planetes',
                'choice_label' => 'nomplanete',
                'multiple'     => false,
                'expanded'     => false,
                'placeholder' => 'None',
                'empty_data' => null,
                'required' => false,
              ))
            ->add('save',      SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Personnages'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_personnages';
    }


}
