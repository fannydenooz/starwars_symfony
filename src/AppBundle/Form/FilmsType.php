<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;


class FilmsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomfilm',      TextType::class)
            ->add('numerodefilm', IntegerType::class, array(
                'label' => 'Numero du film (facultatif)',
                'required' => false
             ))
            ->add('annee', DateType::class, array(
                'label' => 'Date de sortie',
                'years' => range(date('1970'), date('Y')),
                'required' => true
             ))
            ->add('synopsis',   TextareaType::class)
            ->add('file',     FileType::class)            
            ->add('temporalite',   EntityType::class, array(
                'class'        => 'AppBundle:Temporalites',
                'choice_label' => 'nomtemporalite',
                'multiple'     => false,
                'expanded'     => false,
              ))
            ->add('save',      SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Films'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_films';
    }


}
