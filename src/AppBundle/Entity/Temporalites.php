<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Temporalites
 *
 * @ORM\Table(name="temporalites")
 * @ORM\Entity
 */
class Temporalites
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdTemporalite", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtemporalite;

    /**
     * @var string
     *
     * @ORM\Column(name="UrlImage", type="string", length=250, nullable=false)
     */
    private $urlimage;

    /**
     * @var string
     *
     * @ORM\Column(name="NomTemporalite", type="string", length=45, nullable=false)
     */
    private $nomtemporalite;




    /**
     * Get idtemporalite
     *
     * @return integer
     */
    public function getIdtemporalite()
    {
        return $this->idtemporalite;
    }

    /**
     * Set urlimage
     *
     * @param string $urlimage
     *
     * @return Temporalites
     */
    public function setUrlimage($urlimage)
    {
        $this->urlimage = $urlimage;

        return $this;
    }

    /**
     * Get urlimage
     *
     * @return string
     */
    public function getUrlimage()
    {
        return $this->urlimage;
    }

    /**
     * Set nomtemporalite
     *
     * @param string $nomtemporalite
     *
     * @return Temporalites
     */
    public function setNomtemporalite($nomtemporalite)
    {
        $this->nomtemporalite = $nomtemporalite;

        return $this;
    }

    /**
     * Get nomtemporalite
     *
     * @return string
     */
    public function getNomtemporalite()
    {
        return $this->nomtemporalite;
    }

    public function __toString()
    {
        return $this->nomtemporalite;
    }
}
