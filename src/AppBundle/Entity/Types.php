<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Types
 *
 * @ORM\Table(name="types")
 * @ORM\Entity
 */
class Types
{

    /**
     * @var integer
     *
     * @ORM\Column(name="IdType", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtype;

    /**
     * @var string
     *
     * @ORM\Column(name="UrlImage", type="string", length=250, nullable=false)
     */
    private $urlimage;

    /**
     * @var string
     *
     * @ORM\Column(name="NomType", type="string", length=45, nullable=false)
     */
    private $nomtype;



    /**
     * Get idtype
     *
     * @return integer
     */
    public function getIdtype()
    {
        return $this->idtype;
    }

    /**
     * Set urlimage
     *
     * @param string $urlimage
     *
     * @return Types
     */
    public function setUrlimage($urlimage)
    {
        $this->urlimage = $urlimage;

        return $this;
    }

    /**
     * Get urlimage
     *
     * @return string
     */
    public function getUrlimage()
    {
        return $this->urlimage;
    }

    /**
     * Set nomtype
     *
     * @param string $nomtype
     *
     * @return Types
     */
    public function setNomtype($nomtype)
    {
        $this->nomtype = $nomtype;

        return $this;
    }

    /**
     * Get nomtype
     *
     * @return string
     */
    public function getNomtype()
    {
        return $this->nomtype;
    }

    public function __toString()
    {
        return $this->nomtype;
    }
}
