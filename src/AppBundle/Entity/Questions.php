<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Reponses;

/**
 * Questions
 *
 * @ORM\Table(name="questions")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionsRepository")
 */
class Questions
{
    /**
     * @var string
     *
     * @ORM\Column(name="IntituleQuestion", type="string", length=250, nullable=false)
     */
    private $intitulequestion;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdQuestion", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idquestion;

    /**
     * @var \AppBundle\Entity\Thematiques
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Thematiques")
     * @ORM\JoinColumn(nullable=false, name="Thematique", referencedColumnName="IdThematique")
     */
    private $thematique;


    /**
   * @ORM\OneToMany(targetEntity="AppBundle\Entity\Reponses", mappedBy="question",cascade={"all"})
   */
  private $reponses;



    /**
     * Set intitulequestion
     *
     * @param string $intitulequestion
     *
     * @return Questions
     */
    public function setIntitulequestion($intitulequestion)
    {
        $this->intitulequestion = $intitulequestion;

        return $this;
    }

    /**
     * Get intitulequestion
     *
     * @return string
     */
    public function getIntitulequestion()
    {
        return $this->intitulequestion;
    }

    /**
     * Get idquestion
     *
     * @return integer
     */
    public function getIdquestion()
    {
        return $this->idquestion;
    }

        /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->idquestion;
    }

    /**
     * Set thematique
     *
     * @param \AppBundle\Entity\Thematiques $thematique
     *
     * @return Questions
     */
    public function setThematique(\AppBundle\Entity\Thematiques $thematique)
    {
        $this->thematique = $thematique;

        return $this;
    }

    /**
     * Get thematique
     *
     * @return \AppBundle\Entity\Thematiques
     */
    public function getThematique()
    {
        return $this->thematique;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->reponses = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add reponse
     *
     * @param \AppBundle\Entity\Reponses $reponse
     *
     * @return Questions
     */
    public function addReponse(\AppBundle\Entity\Reponses $reponse)
    {
        $reponse->setQuestion($this);
        $this->reponses[] = $reponse;
        

        return $this;
    }

    /**
     * Remove reponse
     *
     * @param \AppBundle\Entity\Reponses $reponse
     */
    public function removeReponse(\AppBundle\Entity\Reponses $reponse)
    {
        $this->reponses->removeElement($reponse);
    }

    /**
     * Get reponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getReponses()
    {
        return $this->reponses;
    }

    public function __toString()
    {
        return $this->intitulequestion;
    }
}
