<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Personnages
 *
 * @ORM\Table(name="personnages")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PersonnagesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Personnages
{
    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=45, nullable=false)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="UrlImage", type="string", length=250, nullable=false)
     */
    private $urlimage;


    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdPersonnage", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idpersonnage;

    /**
     * @var \AppBundle\Entity\Planetes
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Planetes")
     * @ORM\JoinColumn(name="PlaneteOrigine", referencedColumnName="IdPlanete")
     */
    private $planeteorigine;

    /**
     * @var \AppBundle\Entity\Types
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Types")
     * @ORM\JoinColumn(nullable=false, name="Type", referencedColumnName="IdType")
     */
    private $type;


    /*********IMAGE FILE********/
    /**
     * @Assert\Image()
     */
    private $file;

    // On ajoute cet attribut pour y stocker le nom du fichier temporairement
    private $tempFilename;
    
    public function getFile()
    {
        return $this->file;
    }

    // On modifie le setter de File, pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre
    public function setFile(UploadedFile $file)
    {
      $this->file = $file;
  
      // On vérifie si on avait déjà un fichier pour cette entité
      if (null !== $this->urlimage) {
        // On sauvegarde l'extension du fichier pour le supprimer plus tard
        $this->tempFilename = $this->urlimage;
  
        // On réinitialise les valeurs des attributs url 
        $this->urlimage = null;
      }
    }
  
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
      // Si jamais il n'y a pas de fichier (champ facultatif), on ne fait rien
      if (null === $this->file) {
        return;
      }
  
      //defini l'url
      $filename = uniqid('img');
      $this->urlimage = $this->getUploadDir(). '/'. $filename . '.' . $this->file->guessExtension();
  
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
      // Si jamais il n'y a pas de fichier (champ facultatif), on ne fait rien
      if (null === $this->file) {
        return;
      }
  
      // Si on avait un ancien fichier, on le supprime
      if (null !== $this->tempFilename) {
        $oldFile = $this->getRootDir().$this->tempFilename;
        if (file_exists($oldFile)) {
          unlink($oldFile);
        }
      }
  
      //on récupère le nom du fichier depuis l'url
      $nomparse=explode('/' , $this->urlimage);
      $filename = end($nomparse);

      // On déplace le fichier envoyé dans le répertoire de notre choix
      $this->file->move(
        $this->getUploadRootDir(), // Le répertoire de destination
        $filename   // Le nom du fichier à créer
      );
    }
  
    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
      // On sauvegarde temporairement le nom du fichier
      $this->tempFilename = $this->getRootDir().$this->urlimage;
    }
  
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
      // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
      if (file_exists($this->tempFilename)) {
        // On supprime le fichier
        unlink($this->tempFilename);
      }
    }
  
    public function getUploadDir()
    {
      // On retourne le chemin relatif vers l'image pour un navigateur
      return 'images/personnages';
    }
  
    protected function getRootDir()
    {
      // On retourne le chemin relatif vers l'image pour notre code PHP
      return __DIR__.'/../../../web/';
    }
  
    protected function getUploadRootDir()
    {
      // On retourne le chemin relatif vers l'image pour notre code PHP
      return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    /*********END IMAGE FILE********/


    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Personnages
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }


    /**
     * Set urlimage
     *
     * @param string $urlimage
     *
     * @return Personnages
     */
    public function setUrlimage($urlimage)
    {
        $this->urlimage = $urlimage;

        return $this;
    }

    /**
     * Get urlimage
     *
     * @return string
     */
    public function getUrlimage()
    {
        return $this->urlimage;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Personnages
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get idpersonnage
     *
     * @return integer
     */
    public function getIdpersonnage()
    {
        return $this->idpersonnage;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->idpersonnage;
    }

    /**
     * Set planeteorigine
     *
     * @param \AppBundle\Entity\Planetes $planeteorigine
     *
     * @return Personnages
     */
    public function setPlaneteorigine(\AppBundle\Entity\Planetes $planeteorigine = null)
    {
        $this->planeteorigine = $planeteorigine;

        return $this;
    }

    /**
     * Get planeteorigine
     *
     * @return \AppBundle\Entity\Planetes
     */
    public function getPlaneteorigine()
    {
        return $this->planeteorigine;
    }

    /**
     * Set type
     *
     * @param \AppBundle\Entity\Types $type
     *
     * @return Personnages
     */
    public function setType(\AppBundle\Entity\Types $type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AppBundle\Entity\Types
     */
    public function getType()
    {
        return $this->type;
    }

    public function __toString()
    {
        return $this->nom;
    }

}
