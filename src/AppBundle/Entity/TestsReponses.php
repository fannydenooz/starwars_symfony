<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TestsReponses
 *
 * @ORM\Table(name="tests_reponses")
 * @ORM\Entity
 */
class TestsReponses
{
    /**
     * @var integer
     *
     * @ORM\Column(name="IdTestsReponses", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtestsreponses;

    /**
     * @var \AppBundle\Entity\Reponses
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Reponses")
     * @ORM\JoinColumn(nullable=false, name="Reponse", referencedColumnName="IdReponse")
     */
    private $reponse;

    /**
     * @var \AppBundle\Entity\Tests
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Tests", inversedBy="tests")
     * @ORM\JoinColumn(nullable=false, name="Test", referencedColumnName="IdTest")
     */
    private $test;



    /**
     * Get idtestsreponses
     *
     * @return integer
     */
    public function getIdtestsreponses()
    {
        return $this->idtestsreponses;
    }

    /**
     * Set reponse
     *
     * @param \AppBundle\Entity\Reponses $reponse
     *
     * @return TestsReponses
     */
    public function setReponse(\AppBundle\Entity\Reponses $reponse)
    {
        $this->reponse = $reponse;

        return $this;
    }

    /**
     * Get reponse
     *
     * @return \AppBundle\Entity\Reponses
     */
    public function getReponse()
    {
        return $this->reponse;
    }

    /**
     * Set test
     *
     * @param \AppBundle\Entity\Tests $test
     *
     * @return TestsReponses
     */
    public function setTest(\AppBundle\Entity\Tests $test)
    {
        $this->test = $test;

        return $this;
    }

    /**
     * Get test
     *
     * @return \AppBundle\Entity\Tests
     */
    public function getTest()
    {
        return $this->test;
    }
}
