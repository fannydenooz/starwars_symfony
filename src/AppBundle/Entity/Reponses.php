<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reponses
 *
 * @ORM\Table(name="reponses")
 * @ORM\Entity
 */
class Reponses
{
    /**
     * @var string
     *
     * @ORM\Column(name="IntituleReponse", type="string", length=250, nullable=false)
     */
    private $intitulereponse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="BonneReponse", type="boolean", nullable=false)
     */
    private $bonnereponse;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdReponse", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idreponse;

    /**
     * @var \AppBundle\Entity\Questions
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Questions", inversedBy="reponses",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false, name="Question", referencedColumnName="IdQuestion")
     */
    private $question;




    /**
     * Set intitulereponse
     *
     * @param string $intitulereponse
     *
     * @return Reponses
     */
    public function setIntitulereponse($intitulereponse)
    {
        $this->intitulereponse = $intitulereponse;

        return $this;
    }

    /**
     * Get intitulereponse
     *
     * @return string
     */
    public function getIntitulereponse()
    {
        return $this->intitulereponse;
    }

    /**
     * Set bonnereponse
     *
     * @param boolean $bonnereponse
     *
     * @return Reponses
     */
    public function setBonnereponse($bonnereponse)
    {
        $this->bonnereponse = $bonnereponse;

        return $this;
    }

    /**
     * Get bonnereponse
     *
     * @return boolean
     */
    public function getBonnereponse()
    {
        return $this->bonnereponse;
    }

    /**
     * Get idreponse
     *
     * @return integer
     */
    public function getIdreponse()
    {
        return $this->idreponse;
    }

    /**
     * Set question
     *
     * @param \AppBundle\Entity\Questions $question
     *
     * @return Reponses
     */
    public function setQuestion(\AppBundle\Entity\Questions $question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return \AppBundle\Entity\Questions
     */
    public function getQuestion()
    {
        return $this->question;
    }

    public function __toString()
    {
        return $this->intitulereponse;
    }
}
