<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Thematiques
 *
 * @ORM\Table(name="thematiques")
 * @ORM\Entity
 */
class Thematiques
{

    /**
     * @var integer
     *
     * @ORM\Column(name="IdThematique", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idthematique;

    /**
     * @var string
     *
     * @ORM\Column(name="NomThematique", type="string", length=45, nullable=false)
     */
    private $nomthematique;



    /**
     * Get idthematique
     *
     * @return integer
     */
    public function getIdthematique()
    {
        return $this->idthematique;
    }

    /**
     * Set nomthematique
     *
     * @param string $nomthematique
     *
     * @return Thematiques
     */
    public function setNomthematique($nomthematique)
    {
        $this->nomthematique = $nomthematique;

        return $this;
    }

    /**
     * Get nomthematique
     *
     * @return string
     */
    public function getNomthematique()
    {
        return $this->nomthematique;
    }

    public function __toString()
    {
        return $this->nomthematique;
    }
}
