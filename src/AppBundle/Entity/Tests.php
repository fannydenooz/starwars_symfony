<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use UserBundle\Entity\User;

/**
 * Tests
 *
 * @ORM\Table(name="tests")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TestsRepository")
 */
class Tests
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="date", nullable=false)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="Resultat", type="integer", nullable=true)
     */
    private $resultat;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdTest", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idtest;

    /**
     * @var \UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=false, name="User", referencedColumnName="id")
     */
    private $user;


    /**
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\TestsReponses", mappedBy="test")
    */
    private $testsreponses;


    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Tests
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set resultat
     *
     * @param integer $resultat
     *
     * @return Tests
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;

        return $this;
    }

    /**
     * Get resultat
     *
     * @return integer
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * Get idtest
     *
     * @return integer
     */
    public function getIdtest()
    {
        return $this->idtest;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\User $user
     *
     * @return Tests
     */
    public function setUser(\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->testsreponses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \DateTime("now");
    }

    /**
     * Add testsreponse
     *
     * @param \AppBundle\Entity\TestsReponses $testsreponse
     *
     * @return Tests
     */
    public function addTestsreponse(\AppBundle\Entity\TestsReponses $testsreponse)
    {
        $testsreponse->setTest($this);
        
        $this->testsreponses[] = $testsreponse;

        return $this;
    }

    /**
     * Remove testsreponse
     *
     * @param \AppBundle\Entity\TestsReponses $testsreponse
     */
    public function removeTestsreponse(\AppBundle\Entity\TestsReponses $testsreponse)
    {
        $this->testsreponses->removeElement($testsreponse);
    }

    /**
     * Get testsreponses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestsreponses()
    {
        return $this->testsreponses;
    }
}
