<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Films
 *
 * @ORM\Table(name="films")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FilmsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Films
{
    /**
     * @var string
     *
     * @ORM\Column(name="NomFilm", type="string", length=100, nullable=false)
     */
    private $nomfilm;

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroDeFilm", type="integer", nullable=true)
     */
    private $numerodefilm;

    /**
     * @var string
     *
     * @ORM\Column(name="Synopsis", type="text", length=65535, nullable=true)
     */
    private $synopsis;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Annee", type="date", nullable=false)
     */
    private $annee;

    /**
     * @var string
     *
     * @ORM\Column(name="UrlImage", type="string", length=250, nullable=false)
     */
    private $urlimage;

    /**
     * @var integer
     *
     * @ORM\Column(name="IdFilm", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idfilm;

    /**
     * @var \AppBundle\Entity\Temporalites
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Temporalites")
     * @ORM\JoinColumn(nullable=false, name="Temporalite", referencedColumnName="IdTemporalite")
     */
    private $temporalite;


    /*********IMAGE FILE********/
    /**
     * @Assert\Image()
     */
    private $file;

    // On ajoute cet attribut pour y stocker le nom du fichier temporairement
    private $tempFilename;
    
    public function getFile()
    {
        return $this->file;
    }

    // On modifie le setter de File, pour prendre en compte l'upload d'un fichier lorsqu'il en existe déjà un autre
    public function setFile(UploadedFile $file)
    {
      $this->file = $file;
  
      // On vérifie si on avait déjà un fichier pour cette entité
      if (null !== $this->urlimage) {
        // On sauvegarde l'extension du fichier pour le supprimer plus tard
        $this->tempFilename = $this->urlimage;
  
        // On réinitialise les valeurs des attributs url 
        $this->urlimage = null;
      }
    }
  
    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload()
    {
      // Si jamais il n'y a pas de fichier (champ facultatif), on ne fait rien
      if (null === $this->file) {
        return;
      }
  
      //defini l'url
      $filename = uniqid('img');
      $this->urlimage = $this->getUploadDir(). '/'. $filename . '.' . $this->file->guessExtension();
  
    }

    /**
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload()
    {
      // Si jamais il n'y a pas de fichier (champ facultatif), on ne fait rien
      if (null === $this->file) {
        return;
      }
  
      // Si on avait un ancien fichier, on le supprime
      if (null !== $this->tempFilename) {
        $oldFile = $this->getRootDir().$this->tempFilename;
        if (file_exists($oldFile)) {
          unlink($oldFile);
        }
      }
  
      //on récupère le nom du fichier depuis l'url
      $nomparse=explode('/' , $this->urlimage);
      $filename = end($nomparse);

      // On déplace le fichier envoyé dans le répertoire de notre choix
      $this->file->move(
        $this->getUploadRootDir(), // Le répertoire de destination
        $filename   // Le nom du fichier à créer
      );
    }
  
    /**
     * @ORM\PreRemove()
     */
    public function preRemoveUpload()
    {
      // On sauvegarde temporairement le nom du fichier
      $this->tempFilename = $this->getRootDir().$this->urlimage;
    }
  
    /**
     * @ORM\PostRemove()
     */
    public function removeUpload()
    {
      // En PostRemove, on n'a pas accès à l'id, on utilise notre nom sauvegardé
      if (file_exists($this->tempFilename)) {
        // On supprime le fichier
        unlink($this->tempFilename);
      }
    }
  
    public function getUploadDir()
    {
      // On retourne le chemin relatif vers l'image pour un navigateur
      return 'images/films';
    }
  
    protected function getRootDir()
    {
      // On retourne le chemin relatif vers l'image pour notre code PHP
      return __DIR__.'/../../../web/';
    }
  
    protected function getUploadRootDir()
    {
      // On retourne le chemin relatif vers l'image pour notre code PHP
      return __DIR__.'/../../../web/'.$this->getUploadDir();
    }

    /*********END IMAGE FILE********/

    /**
     * Set nomfilm
     *
     * @param string $nomfilm
     *
     * @return Films
     */
    public function setNomfilm($nomfilm)
    {
        $this->nomfilm = $nomfilm;

        return $this;
    }

    /**
     * Get nomfilm
     *
     * @return string
     */
    public function getNomfilm()
    {
        return $this->nomfilm;
    }

    /**
     * Set numerodefilm
     *
     * @param integer $numerodefilm
     *
     * @return Films
     */
    public function setNumerodefilm($numerodefilm)
    {
        $this->numerodefilm = $numerodefilm;

        return $this;
    }

    /**
     * Get numerodefilm
     *
     * @return integer
     */
    public function getNumerodefilm()
    {
        return $this->numerodefilm;
    }

    /**
     * Set synopsis
     *
     * @param string $synopsis
     *
     * @return Films
     */
    public function setSynopsis($synopsis)
    {
        $this->synopsis = $synopsis;

        return $this;
    }

    /**
     * Get synopsis
     *
     * @return string
     */
    public function getSynopsis()
    {
        return $this->synopsis;
    }

    /**
     * Set annee
     *
     * @param \DateTime $annee
     *
     * @return Films
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Get annee
     *
     * @return \DateTime
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Set urlimage
     *
     * @param string $urlimage
     *
     * @return Films
     */
    public function setUrlimage($urlimage)
    {
        $this->urlimage = $urlimage;

        return $this;
    }

    /**
     * Get urlimage
     *
     * @return string
     */
    public function getUrlimage()
    {
        return $this->urlimage;
    }

    /**
     * Get idfilm
     *
     * @return integer
     */
    public function getIdfilm()
    {
        return $this->idfilm;
    }

      /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->idfilm;
    }

    /**
     * Set temporalite
     *
     * @param \AppBundle\Entity\Temporalites $temporalite
     *
     * @return Films
     */
    public function setTemporalite(\AppBundle\Entity\Temporalites $temporalite)
    {
        $this->temporalite = $temporalite;

        return $this;
    }

    /**
     * Get temporalite
     *
     * @return \AppBundle\Entity\Temporalites
     */
    public function getTemporalite()
    {
        return $this->temporalite;
    }

    public function __toString()
    {
        return $this->nomfilm;
    }

}
